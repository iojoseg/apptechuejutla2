package mx.tecnm.misantla.myapptechuejutla2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnVerificar.setOnClickListener {

            var anio = edtAnio.text.toString()

            if(anio.isEmpty()){
                Toast.makeText(this,"Debe ingresar su año de nacimiento",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            var anio2 = edtAnio.text.toString().toInt()

            when (anio2) {
                in 1930..1948 -> {
                    tvResultado.text = "Silent Generation"

                }
                in 1949..1968 -> {
                    tvResultado.text = "Baby Boom"
                }
                in 1969..1980 -> {
                    tvResultado.text = "Generation X"
                }
                in 1981..1993 -> {
                    tvResultado.text = "Generation Y - Millennials - "
                    imageView.setImageResource(R.drawable.uno)
                }
                in 1994..2010 -> {
                    tvResultado.text = "Generation Z "
                }
                else -> {
                    tvResultado.text = "No tiene Generacion"
                }
            }

        }
    }
}